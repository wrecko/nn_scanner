
import pandas as pd
import re
import time
import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
from torch import optim

from models.PriceModel import Model


TREND_INTERVAL = 60

dataset_train = pd.read_csv('Google_Stock_Price_Train.csv')
training_set = dataset_train.iloc[:, 4:5].values
for i in range(len(training_set)):    
    if ',' in training_set[i][0]:
        training_set[i][0] = training_set[i][0].replace(',', '')
    training_set[i][0] = float(training_set[i][0])
        
# Feature Scaling
from sklearn.preprocessing import MinMaxScaler
sc = MinMaxScaler(feature_range = (0, 1))
training_set_scaled = sc.fit_transform(training_set)

X_train = []
y_train = []
for i in range(TREND_INTERVAL, len(training_set)):    
    X_train.append(training_set[i-TREND_INTERVAL:i, 0])
    y_train.append(training_set[i])

X_train = torch.Tensor(X_train)
y_train = torch.Tensor(y_train)

X_train = X_train.view(len(X_train), TREND_INTERVAL, -1)

hidden_layer = 64
learning_rate = 0.01
batch_size = 32
epochs = 100
report_interval = epochs/10

if torch.cuda.is_available():
    device = torch.device("cuda")
    use_cuda = True
    print('Cuda enabled')
else:
    device = torch.device("cpu")
    use_cuda = False
    
if use_cuda:
	model = None
else:
	model = Model(60, hidden_layer, output_size= 1)
    
loss_function = nn.MSELoss(size_average=False)
optimizer = optim.Adam(model.parameters(), lr= learning_rate)

avg_loss = 0  
print("Training starts!!!")
for i in range(epochs):    
    
    model.zero_grad()
    y_pred = model(X_train)
    loss = loss_function(y_pred, y_train)
    avg_loss += loss.data.item()
    
    
    loss.backward()
    optimizer.step()
    
    if i % report_interval == 0:
        print("Epoch: {}, Avg loss: {}, Time: {}".format(i, avg_loss, time.ctime(int(time.time()))))        


# Getting the real stock price of 2017
dataset_test = pd.read_csv('Google_Stock_Price_Test.csv')
real_stock_price = dataset_test.iloc[:, 4:5].values

# Getting the predicted stock price of 2017
dataset_total = pd.concat((dataset_train['Close'], dataset_test['Close']), axis = 0)
inputs = dataset_total[len(dataset_total) - len(dataset_test) - 60:].values
inputs = inputs.reshape(-1,1)
inputs = sc.transform(inputs)
X_test = []
for i in range(60, 80):
    X_test.append(inputs[i-60:i, 0])
X_test = torch.Tensor(X_test)
X_test = X_test.view(len(X_test), TREND_INTERVAL, -1)



predicted = []
for i in X_test:    
    y_pred = model(i.view(1, -1))
    predicted.append(y_pred.data.item())
    
predicted = sc.inverse_transform(np.reshape(predicted, (-1,1)))


# Visualising the results
plt.plot(real_stock_price, color = 'red', label = 'Real Google Stock Price')
plt.plot(predicted, color = 'blue', label = 'Predicted Google Stock Price')
plt.title('Google Stock Price Prediction')
plt.xlabel('Time')
plt.ylabel('Google Stock Price')
plt.legend()
plt.show()