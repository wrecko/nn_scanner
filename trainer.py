# Importing the libraries
import pandas as pd
import math
import time

import torch
import torch.nn as nn
from torch import optim

from model import Model
import utils
from utils import TextProcesser


  
DATA_PATH = 'data/'

X = []
Y = []

# Data process
#############################################################################
print(" Loading data ")

dataset = pd.read_csv('news_data.csv', delimiter=',')
trainSetLenght = round(len(dataset) * 1)
for index, row in dataset.iterrows():
    if index > trainSetLenght:
        break
    #print(row['text'], row['perc'])
    Y.append(utils.classify_data(row['perc']))
    symbol = row['text'].split('-')[0]
    
    with open(DATA_PATH+symbol+'/'+row['text']+'.txt', 'r') as file:
        X.append(file.read())
    


# Stop word and Lemmantization processing 
# word to index
wordProcesser = TextProcesser()
print('Dataset size: {}'.format(len(X)))
for i in range(len(X)):
    X[i] = wordProcesser.processText(X[i])
    #X[i] = wordProcesser.indexWords(X[i])
    X[i] = wordProcesser.indexLetters(X[i])
#    X[i] = torch.LongTensor(X[i])

#X = [X[i:i + 32] for i in range(0, len(X), 32)]
#XX = torch.tensor(X)
#
#Y = [Y[i:i + 32] for i in range(0, len(Y), 32)]
#Y = torch.tensor(Y[0:-1])

# NN Training
############################################################################
test_model_epoch = False
report_interval = 15000  
max_sequence_len = 3000
vocabSize = len(wordProcesser.dict)
hidden_layer = 64
embeding_layer = 64
learning_rate = 0.001
batch_size = 32
epochs = 5

if torch.cuda.is_available():
    device = torch.device("cuda")
    use_cuda = True
    print('Cuda enabled')
else:
    device = torch.device("cpu")
    use_cuda = False
    
if use_cuda:
	model = Model(embeding_layer, hidden_layer, vocabSize, len(utils.classes)).cuda()
else:
	model = Model(embeding_layer, hidden_layer, vocabSize, len(utils.classes))
   
#.load_state_dict(torch.load('models/model.pth'))

loss_function = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr= learning_rate)

print('starting training')

nanTermination = False
start = 0
epochTime = 0
model_iter = 1
lowest_loss = 0
start = time.time()
for i in range(epochs):
    if nanTermination:
        print('nan termination')
        break
    avg_loss = 0.0    
    epochTime = time.time()
    for idx, lines in enumerate(X):         
        if lines == []: continue
#        if len(lines) > max_sequence_len:
#            lines = lines[0:max_sequence_len]
#            #print(lines)
        
        data = torch.LongTensor(lines)
        label = torch.LongTensor([Y[idx]])
       
        model.zero_grad()
        y_pred = model(data)
        
        loss = loss_function(y_pred, label)
        avg_loss += loss.data.item()
        #print(avg_loss)
#        avg_loss += (torch.max(prediction, 1)[1].view(target.size()).data == target.data).sum()
#        if math.isnan(loss.data.item()):
#            nanTermination = True
#            break
        
        loss.backward()
        optimizer.step()
         
        if idx%report_interval == 0:            
            #print(y_pred)
            print('epoch: {} iterations: {} loss: {} time: {}, {}'.format((i+1), idx, round(loss.data.item(), 8), round(time.time()-start), time.ctime(int(time.time()))))
            start = time.time()
    
    if math.isnan(avg_loss/len(X)):
        print('NaN Termination ' + time.ctime(int(time.time())))
        torch.save(model.state_dict(), 'saved_models/models/model_ver' + str(model_iter)+'.pth')   
        break
            
    print('epoch: {} iterations: {} loss: {} time: {}, {}'.format((i+1), idx, round(loss.data.item(), 8), round(time.time()-start), time.ctime(int(time.time())))) 
    print("avg loss after completion of {} epochs is {}, time {}, {}".format((i+1),(avg_loss/len(X)), round(time.time() - epochTime), time.ctime(int(time.time()))))
    
    if test_model_epoch:
        pass
    
    
#    if lowest_loss == 0 or lowest_loss > avg_loss/len(X):
#        lowest_loss = avg_loss/len(X)
#        torch.save(model.state_dict(), 'models/model_ver' + str(model_iter)+'.pth')	
#        print('Saving model: model_ver{}'.format(str(model_iter)))
#        model_iter += 1
    torch.save(model.state_dict(), 'saved_models/model_ver' + str(model_iter)+'.pth')   
    model_iter += 1
    
#with open('dict.pkl','wb') as f :
#	pickle.dump(wordIn.word_to_idx,f)






  
#for index, row in dataset.iterrows():
##    print(row['text'], row['perc'])
#    result.append(temp(row['perc']))
#    symbol = row['text'].split('-')[0]
#    
#    with open(DATA_PATH+symbol+'/'+row['text']+'.txt', 'r') as file:
#        data.append(file.read())
#    
#with open('output.tsv', 'wt') as out_file:
#    i = 0
#    tsv_writer = csv.writer(out_file, delimiter='\t')
#    for text in range(len(data)):
#        tsv_writer.writerow([data[i], result[i]])
#        i += 1
        
#######################################################################################        

# Importing the dataset
#dataset = pd.read_csv('output.tsv', delimiter='\t', names = ["body", "vals"])   
#train_size = round(len(dataset)*0.8) 
#train = dataset[0:train_size]
#test = dataset[train_size: len(dataset)]
#print(len(train)+len(test) == len(dataset))
#
#
#with open('train.tsv', 'wt') as out_file:
#    i = 0
#    tsv_writer = csv.writer(out_file, delimiter='\t')
#    for text in range(len(train)):
#        tsv_writer.writerow([train.iloc[i][0], train.iloc[i][1]])
#        i += 1
#with open('test.tsv', 'wt') as out_file:
#    i = 0
#    tsv_writer = csv.writer(out_file, delimiter='\t')
#    for text in range(len(test)):
#        tsv_writer.writerow([test.iloc[i][0], test.iloc[i][1]])
#        i += 1



