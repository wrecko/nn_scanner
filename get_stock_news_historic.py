import requests
from parsel import Selector
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import pandas as pd
import time
from db_handler import DbHandler
import os


host = 'https://www.londonstockexchange.com'
cookies = {'rns_cookie': 'XFAeVf0jddSqU3HXccbiqTdMLEmcSJJZ'}

def saveToFile(sym, fileName, data):
    dirPath = 'data/'+sym    
    if os.path.isdir(dirPath) == False:
        os.makedirs(dirPath)
        
    with open(dirPath+'/'+fileName+'.txt', 'w+') as file:        
        file.write(data)
        print('Saving data: '+fileName)


def cleanhtml(raw_html):
    result = []
    ps = PorterStemmer()
    for html in raw_html: 
        if 'javascript' in html:
            continue
        cleanr = re.compile('<.*?>')
        cleanHtml = re.sub(cleanr, '', html)
        cleanHtml = re.sub('[^a-zA-Z0-9]', ' ', cleanHtml)        
        cleanHtml = cleanHtml.lower()             
        result += cleanHtml.split()
    
#    result = [ps.stem(word) for word in result if not word in set(stopwords.words('english'))]    
    return ' '.join(result)


nltk.download('stopwords')
db = DbHandler()
symbols = db.GetSymbols()


for symbol in symbols:
#    url = input('Enter URL:')
    ID = symbol[0]
    dbSym = symbol[2]
    
    if '.L' in dbSym:
            dbSym = dbSym.replace('.L', '')
    dbNewsLink = symbol[3]
    
    # Find data if new stock in etoro
    if dbNewsLink == None:        
        url = 'https://www.londonstockexchange.com/exchange/prices-and-markets/stocks/prices-search/stock-prices-search.html?nameCode='+dbSym.lower()
        
        symRequest = requests.get(url)
        sel = Selector(text = symRequest.text)
        tmp = sel.css('td').css('a::attr(href)')[0].get()
        dbNewsLink = 'https://www.londonstockexchange.com/exchange/prices-and-markets/stocks/exchange-insight/news-analysis.html?fourWayKey='+tmp[tmp.index('-summary/')+9:-5]
        print('Symbol link: '+dbNewsLink)
        db.InsertSymbolNewsLink(ID, dbNewsLink)
        print('save')
    
    
    test = requests.get(dbNewsLink)    
    nextPage = ' ';
    
    resultText = []
    resultPerc = []
    
    while nextPage is not None:
        sel = Selector(text = test.text)
        nextPage = sel.xpath("//a[contains(., 'Next')]").get()
#        print(nextPage)
        if nextPage is not None:
            nextPage = nextPage[nextPage.index('ref=')+5: nextPage.index(' title')-1]    
#            print('Next page: ' + (host+nextPage).replace('&amp;', '&'))
            test = requests.get((host+nextPage).replace("&amp;", "&")) 
        
        newsList = sel.css('tbody[class=table_datinews]')
        
        links = newsList.css('a::attr(href)').getall()
        percs = newsList.css('td').getall()[4::5]        
        
        linksParsed = []
        i = 0        
        
        stop = False
        
        for link in links:  
            try:
            
                newsID = dbSym+'-'+ link[link.index(dbSym)+len(dbSym)+1:link.index('News')-9]
                if '-/' in newsID:
                    newsID = newsID.replace('-/', '-')
#                if db.IfExistsNews(newsID):
#                    print('Break stop')
#                    stop = True
#                    break
                
                newsLink = host+link[link.index('openWin2')+10: link.index('News')-4]                 
#                print(newsLink)
                
                perc = percs[i]
                if '>-<' in perc:
                    continue
                
                perc = perc[perc.index('ft')+4: perc.index('%\xa0<')]        
                if perc[0] == '+':
                    perc = perc[1:]
                
                if db.IfExistsNews(newsID) == False:
                    db.InsertNewsDetailLink(newsID, newsLink, perc)
                
                news = requests.get(newsLink, cookies = cookies).text
                _selT = Selector(text = news)
                news = _selT.css('div[id=fullcontainer]').css('p').getall()
                
                news = cleanhtml(news)
            
                saveToFile(dbSym, newsID, news)
                resultText.append(newsID)
                resultPerc.append(perc)
                
                i += 1
                time.sleep(1)
            except Exception as e:
                print(e)
                continue
        
        if stop:
            break
        if nextPage is not None:   
            print('Next page: ' + (host+nextPage).replace('&amp;', '&'))    
        time.sleep(2)
    

    dataset = {'text': resultText, 'perc': resultPerc}    
    df = pd.DataFrame(dataset)
    try:
        df2 = pd.read_csv('news_data.csv')
        df2 = df2.append(df)
        df2.to_csv('news_data.csv', index = False)
    except:
        print('error loading csv file')
        df.to_csv('news_data.csv', index = False)
   
    

