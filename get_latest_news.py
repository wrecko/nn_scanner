import os
import requests
from parsel import Selector
import time

from db_handler import DbHandler
from predictor import Predictor
import utils
from utils import TextProcesser

blacklistTitles = ['Start Of Day Message - Good Morning', 'Test Message', 'Service Notice', 'End Of Day Message - Good Night', 'Start of Day Message', 'Start of Day']           

host = 'https://www.londonstockexchange.com'
cookies = {'rns_cookie': 'XFAeVf0jddSqU3HXccbiqTdMLEmcSJJZ'}

def saveToFile(sym, fileName, data):
    dirPath = 'data/'+sym    
    if os.path.isdir(dirPath) == False:
        os.makedirs(dirPath)
        
    with open(dirPath+'/'+fileName+'.txt', 'w+') as file:        
        file.write(data)
        print('Saving data: '+fileName)

results = []
predictedTitles = []

textProcesser = TextProcesser()
db = DbHandler()
predictor = Predictor('models/last/model_70_3.pth')

while True:
    newsRequest = requests.get(host+'/exchange/news/market-news/market-news-home.html')
    sel = Selector(text = newsRequest.text)
    linkElements = sel.css('td').css('a');
    
    
    for url in linkElements:
        link = url.css('a::attr(href)').get()
        if 'javascript' not in link:
            continue
        
        link = link[link.index('openWin2')+10: link.index('News')-4].strip()
        title = url.css('a::text').get().strip()
        symbol = link[link.index('market-news-detail') + len('market-news-detail') + 1 : -1]
        symbol = symbol[0: symbol.index('/')]
        newsID = symbol+'-'+ link[link.index(symbol)+len(symbol)+1:link.index('html')-1]
        
        if title in blacklistTitles or title in predictedTitles:
            continue
        #print(title)
        newsReq = requests.get(host+link, cookies = cookies).text
        _selT = Selector(text = newsReq)
        news = _selT.css('div[id=fullcontainer]').css('p').getall()        
        news = textProcesser.processRawHtml(news)
        # Prediction        
        output, val = predictor.Predict(news)
        prediction = utils.classes[val]
        # DB save
        db.InsertNewsPrediction(newsID, link, symbol, val)
        # 
        results.append([symbol, title, link, prediction])
        predictedTitles.append(title)
        print('News ID: {}, prediction: {}'.format(newsID, prediction))
    
    time.sleep(3)
    
