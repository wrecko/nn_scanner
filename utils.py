import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import re
import pickle
import math



classes = ['Under 5%',  'Under 1%', 'Under 0%', 'Over 0%', 'Over 1%',  'Over 5%']
#              0            1           2           3           4          5          6          7          8          9  

def classify_data(val):
    val = str(val).replace(',', '')
    val = float(val)
    if math.isnan(val):
        val = 0
        
    if val < -5:
        return 0
#    if val < -3:
#        return 1
#    if val < -2:
#        return 2
    if val < -1:
        return 1
    if val < 0:
        return 2
    if val >= 0 and val < 1:
        return 3
    if val >= 1 and val < 5:
        return 4
#    if val >= 2 and val < 3:
#        return 7
#    if val >= 3 and val < 5:
#        return 8
    if val >= 5:
        return 5
    

dict_path = 'dict.pkl'
class wordIndex(object):
    def __init__(self):
        self.count = 0
        self.word_to_idx = {}
        self.word_count = {}
        self.letters = []
        self.letters.append(' ')
        
    def add_word(self,word) :
        if not word in self.word_to_idx :
            self.word_to_idx[word] = self.count
            self.word_count[word] = 1
            self.count +=1
            
            for letter in word:
                if letter not in self.letters:
                    self.letters.append(letter)
            
        else:
            self.word_count[word]+=1

    def add_text(self,text) :
        for word in text.split(' ') :
            self.add_word(word)
            


class TextProcesser:
    def __init__(self):
        #nltk.download('wordnet')
        
        self.lemmatizer = WordNetLemmatizer()
        self.stopwords = set(stopwords.words('english'))
        
        with open(dict_path, 'rb') as pickle_file:
            self.dict = pickle.load(pickle_file) 
        self.wordIndex = wordIndex()
                
    def processRawHtml(self, inputData):
        result = []    
        for html in inputData: 
            if 'javascript' in html:
                continue
            cleanr = re.compile('<.*?>')
            cleanHtml = re.sub(cleanr, '', html)
            cleanHtml = re.sub('[^a-zA-Z]', ' ', cleanHtml)        
            cleanHtml = cleanHtml.lower()             
            result += cleanHtml.split()
        
        result = [self.lemmatizer.lemmatize(word) for word in result if not word in set(self.stopwords)]    
        return ' '.join(result)
    
    def processText(self, inputData):
        self.wordIndex.add_text(inputData)
        tmp = re.sub('[^a-zA-Z0-9]', ' ', inputData)
        splitted_text = tmp.split()
        return ' '.join([self.lemmatizer.lemmatize(word) for word in splitted_text if word not in self.stopwords])
    
    def indexWords(self, inputData):
        outputData = []
        for word in inputData:
            if word in self.dict:
                outputData.append(self.dict[word])
            else:
                outputData.append(len(self.dict))
        
        return outputData
    
   
    def indexLetters(self, inputData):
        outputData = []        
        for c in inputData:
            outputData.append(self.wordIndex.letters.index(c))        
        
        return outputData
            
            