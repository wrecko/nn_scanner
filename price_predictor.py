
import pandas as pd
import re
import time
import numpy as np
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
from torch import optim

from models.PriceModel import Model



TREND_INTERVAL = 60

dataset = pd.read_csv('BTC-USD.csv')
dataset = dataset.iloc[:, 4:5].values

train_count = 1200
train_set = dataset[:train_count]

X_train = []
y_train = []
for i in range(TREND_INTERVAL, train_count):
    X_train.append(train_set[i-TREND_INTERVAL:i, 0])
    y_train.append(train_set[i])

X_train = torch.Tensor(X_train)
y_train = torch.Tensor(y_train)

#X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
hidden_layer = 64
learning_rate = 0.1
batch_size = 32
epochs = 100
report_interval = epochs/10

if torch.cuda.is_available():
    device = torch.device("cuda")
    use_cuda = True
    print('Cuda enabled')
else:
    device = torch.device("cpu")
    use_cuda = False
        
        
def train(hidden, learningRate):   
        
    if use_cuda:
    	model = None
    else:
    	model = Model(1, hidden, num_layers=2)
        
    loss_function = nn.MSELoss(size_average=False)
    optimizer = optim.Adam(model.parameters(), lr= learning_rate)
    
    allTimeAvgLoss = 0
    print("Training starts!!!")
    for i in range(epochs):
        avg_loss = 0   
        for j in range(len(X_train)): 
            model.zero_grad()
            y_pred = model(X_train[j])
            loss = loss_function(y_pred, y_train[j])
            avg_loss += loss.data.item()
            
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        
        allTimeAvgLoss += avg_loss/len(X_train) 
        if i % report_interval == 0:
            print("Epoch: {}, Avg loss: {}, Time: {}".format(i, avg_loss/len(X_train), time.ctime(int(time.time()))))        
        
    print("Training Ended, Avg Loss: {}, Time: {}".format(allTimeAvgLoss/epochs, time.ctime(int(time.time()))))   
    return model, allTimeAvgLoss/epochs
    #torch.save(model.state_dict(), 'model.pth')  

model, loss = train(32, 0.1)




y = train_set[-1]
test = train_set[-61:]
test = test[:-1]
test = torch.Tensor(test)

hiddenLayers = [32, 64]
learning_rates = [0.01, 0.05, 0.1, 0.5, 1, 2, 3, 5]
numLayers = [1]
result = []

for hidden in hiddenLayers:
    for rate in learning_rates:
        try:
            print("Start training: hidden {}, LR {}".format(hidden, rate))
            model, loss = train(hidden, rate)           
            
            model.zero_grad()
            y_pred = model(test)
            result.append([loss, "Model Hidden: {} LR: {}, LN: {}. Predicted: {}, Real: {}, Loss: {}".format(hidden, rate, 1, y_pred.data.item(), y[0], loss), model])
            
        except:
            print("")
            print("")
            for t in result:
                print(t[1])
            raise Exception("")
            
            
for t in result:
    print(t[1])     
minn = min(result)
print("")
print(minn[1])




#    
#    
#plt.plot(dataset, color = 'red', label = 'Real Google Stock Price')
#plt.plot(train_set, color = 'blue', label = 'Predicted Google Stock Price')
#plt.title('Google Stock Price Prediction')
#plt.xlabel('Time')
#plt.ylabel('Google Stock Price')
#plt.legend()
#plt.show()
    
    


    
    
    
    
    
    
    