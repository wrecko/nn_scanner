import pyodbc
import config

class DbHandler:
    def __init__(self):        
        self.server = 'vrecnik.database.windows.net'
        self.database = 'personal'
        self.table = 'stock.Exchange'
        self.logTable = 'dbo.ApplicationLog'
        self.driver= '{ODBC Driver 13 for SQL Server}'
        self.cnxn = pyodbc.connect('DRIVER='+self.driver+';PORT=1433;SERVER='+self.server+';PORT=1443;DATABASE='+self.database+';UID='+config.dbUsername+';PWD='+config.dbPassword)
        self.cursor = self.cnxn.cursor()
        self.ApplicationName = "SCANNER"

    def Query(self, query):
        query = query.format(self.table)
        print(query)
        with self.cursor.execute(query): 
            self.cnxn.commit()
    
    def Select(self):
        self.cursor.execute("SELECT * FROM "+self.table+" ")        
        return self.cursor.fetchall()

    def SelectById(self, id):
        self.cursor.execute("SELECT * FROM "+self.table+" WHERE Id = "+ str(id) +" ")        
        return self.cursor.fetchall()
    
    def InsertLog(self, typ, msg):
        self.cursor.execute("INSERT INTO "+self.logTable+" (ApplicationName, Type, Message) VALUES ('"+self.ApplicationName+"', '"+typ+"', '"+msg+"')")
        self.cnxn.commit()

    def Insert(self, exc, name):
        self.cursor.execute("INSERT INTO "+self.table+" ( Exchange, Symbol) VALUES ('"+exc+"', '"+name+"')")
        self.cnxn.commit() 
        
    def InsertSymbolNewsLink(self, ID, newslink):
        self.cursor.execute("update "+self.table+" set NewsLink = '" + newslink + "' Where ID = '"+ str(ID) + "' ")
        self.cnxn.commit()  
        
    def InsertNewsDetailLink(self, ID, newslink, perc):
        self.cursor.execute("insert into stock.StockNews (NewsId, NewsLink, Perc) values ('"+ID+"', '"+newslink+"', '"+perc+"')")
        self.cnxn.commit() 
        
    def InsertNewsPrediction(self, ID, newslink, symbol, prediction ):
        self.cursor.execute("insert into stock.StockNews (NewsId, NewsLink, Perc, Symbol, AIPrediction) values ('"+ID+"', '"+newslink+"', '0', '"+symbol+"', '"+prediction+"')")
        self.cnxn.commit() 
    
    def IfExistsNews(self, ID):
        self.cursor.execute("SELECT * FROM stock.StockNews WHERE NewsId = '"+ ID +"' ")        
        return len(self.cursor.fetchall()) > 0
        
    
    def GetSymbols(self):
        self.cursor.execute("SELECT * FROM "+self.table+" WHERE Exchange = 'London'")        
        return self.cursor.fetchall()
        
        
        
        
