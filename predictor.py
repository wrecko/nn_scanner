import pandas as pd
import re
import torch
import time

from model import Model
import utils
from utils import TextProcesser


versions = 5
traalala = 363000

test_embedings = 64
test_hidden = 64


if torch.cuda.is_available():
    device = torch.device("cuda")
    use_cuda = True
else:
    device = torch.device("cpu")
    use_cuda = False

class Predictor:
    def __init__(self, model_path, embeding_layer = 64, hidden_layer = 64):
        
        self.processer = TextProcesser()
                
        if use_cuda:
        	self.model = Model(embeding_layer, hidden_layer, len(self.processer.dict), len(utils.classes)).cuda()
        else:
        	self.model = Model(embeding_layer, hidden_layer, len(self.processer.dict), len(utils.classes))
        
        self.model.load_state_dict(torch.load(model_path))
        
    def Predict(self, text):
        
        inputData = self.processer.indexWords(text)
        
        if use_cuda:
            inputData = torch.cuda.LongTensor(inputData)
        else:
            inputData = torch.LongTensor(inputData)
        
        y_pred = self.model(inputData)
        #print(y_pred)
        out, index = y_pred.data.topk(1)
        return out.item(), index.item()
 
    
def Test(embedings, hidden):    

    DATA_PATH = 'data/'
    
    X = []
    Y = []
    yy = []
    
    #    nltk.download('wordnet')
    
    #dataset = pd.read_csv('output.tsv', delimiter='\t', names = ["body", "vals"])
    #X = dataset.iloc[:, 0].values
    #y = dataset.iloc[:, 1].values
    
    dataset = pd.read_csv('news_data.csv', delimiter=',')
    leng = round(len(dataset) * 0.7)
    for index, row in dataset.iterrows():
        if index < leng:
            continue
        
    #    print(row['text'], row['perc'])
        Y.append(utils.classify_data(row['perc']))
        yy.append(row['perc'])
        symbol = row['text'].split('-')[0]
        
        with open(DATA_PATH+symbol+'/'+row['text']+'.txt', 'r') as file:
            X.append(file.read())
       
    
    # Stop word and Lemmantization processing     
    processer = TextProcesser()
    print(len(X))
    for i in range(len(X)):
        X[i] = processer.processText(X[i])
    
    
    results = []
    for ite in range(1, versions+1):
        start = time.time()
        trained_model_path = 'saved_models/model_ver{}.pth'.format(ite)
        #trained_model_path = 'models/last/model_70_3.pth'
    #    print(trained_model_path)
        predict = Predictor(trained_model_path, embedings, hidden)
        correct = 0
        skipped = 0
        for i in range(len(X)):    
            if X[i] == []: 
                skipped += 1
                continue
            output, val = predict.Predict(X[i])
           # print(val, Y[i])
            if val == Y[i]:
                correct += 1
                
        success = round((correct/(len(X)-skipped))*100, 2)
        results.append(success)
        print('Model ' + str(ite) + ', success ' + str(success) + '%' + ', time elapsed: ' + str(time.time() - start))
    
    print('Best model n: {}, result: {}%'.format(results.index(max(results))+1, max(results)))
    
Test(test_embedings, test_hidden)
