import torch
import torch.nn as nn
from torch.autograd import Variable

import torch.nn.functional as F

if torch.cuda.is_available():
    device = torch.device("cuda")
    use_cuda = True
else:
    device = torch.device("cpu")
    use_cuda = False
    
class Model(torch.nn.Module):
    def __init__(self, embedding_dim, hidden_dim, vocabSize, output_size = 2):
        super(Model, self).__init__()
        
        self.hidden_dim = hidden_dim
        self.batch_size = 1
        self.output_size = output_size
        
        self.embeddings = nn.Embedding(vocabSize, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim)
        self.linearOut = nn.Linear(hidden_dim, self.output_size)
        
#    def forward(self, inputs):
#        hidden = self.init_hidden()
#        x = self.embeddings(inputs).view(len(inputs),1,-1)
#        lstm_out,lstm_h = self.lstm(x, hidden)
#        x = lstm_out[-1]
#        x = self.linearOut(x)
#        x = F.log_softmax(x, dim=1)
#        return x,lstm_h 
        
    def forward(self, input_sentence, batch_size = None):
        input = self.embeddings(input_sentence) 
        input = input.view(len(input_sentence), 1, -1) 
        if batch_size is None:
            h_0 = Variable(torch.zeros(1, self.batch_size, self.hidden_dim)) # Initial hidden state of the LSTM
            c_0 = Variable(torch.zeros(1, self.batch_size, self.hidden_dim)) # Initial cell state of the LSTM
        else:
            h_0 = Variable(torch.zeros(1, batch_size, self.hidden_dim))
            c_0 = Variable(torch.zeros(1, batch_size, self.hidden_dim))
            
        output, (final_hidden_state, final_cell_state) = self.lstm(input, (h_0, c_0))
        
        final_output = self.linearOut(final_hidden_state[-1])
        final_output = F.softmax(final_output, dim=1)
        
        return final_output
    
    
